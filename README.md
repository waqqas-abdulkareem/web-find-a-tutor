Find A Tutor
===================

This repository is the backend implementation of **Find A Tutor**, an app connecting users wanting to learn with users willing to teach. 
It is a blend of [TutorHunt](http://tutorhunt.com) and [Uber](http://uber.com).

----------

Features
-------------

####Courses

- The application lists courses according to Subjects (e.g. Mathematics, Arabic, Physics e.t.c). 

- Each Subject is divided into Topics (e.g. Algebra, Arabic Alphabet, Newton's Laws).

- Each Topic consists of Lessons (e.g. Newton's First Law of motion, Newton's Second Law of motion). 

- Each lesson has a level which can be one of `Pre-Primary`,`Primary`,`Lower Secondary`,`Upper Secondary` or `Bachelors`.

- Course Listings are managed by administrators.

####Registration

- During Registration, the users enters basic info. If the user wishes to teach, he will select lessons and provide documentation to proove that he is qualified to teach those lessons.

- A tutor sets his/her price for teaching a lesson.

- A tutor can teach online via Skype and/or he can visit the student at his residence. A tutor sets the distance he is willing to travel during Registration.

####Setting up an Appointments

- A student logs-in to the app and chooses a lesson and a day for the class to take place.
- The application returns a set of tutors available on that day. The student can choose at most 3 tutors from the list.
- Selected tutors are asked if they are free to teach on that date via a Push Notification
- An appointment is set up between the student and the first tutor to respond.
- Payment will only be transferred to the tutor's account after both the student and the tutor confirm that the lesson has been taught.
- A student can not set up a second appointment if he has a previously unpaid appointment.

Completed Features
-

&#10004; Course Listing

Login & Registration:

&#10004; Login
&#10004; Upload Basic Info
&#10008; Upload Profile Picture
&#10004; Select Lessons To Teach
&#10008; Upload Qualifications
&#10008; Set Price for Lesson

Appointments

&#10004; Find Tutors on a certain day
&#10008; Select Tutors
&#10008; Notify Tutors via Push Notifications
&#10008; Ensure Appointments can not be made by user with unpaid appointments
&#10008; Payment system

Disclaimer
-

This is a hobby project and **NOT** a viable business model! The project exists simply to try out new technologies on the web backend and on the mobile front-end.