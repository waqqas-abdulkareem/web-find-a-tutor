"""find_a_tutor URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import include,url

from rest_framework.routers import DefaultRouter
from rest_framework_jwt.views import obtain_jwt_token
from rest_framework.urlpatterns import format_suffix_patterns

from tutors.views import urls
from tutors.views.courses import *
from tutors.views.profile import *
from tutors.views.appointment import *
from  tutors.views.browser import api_root

#Admin URL Patterns

urlpatterns = [
    url(
        r'^admin/', 
        admin.site.urls
    )
]

#Authentication URL Patterns

urlpatterns += [
    url(
        r'^$',
        api_root
    ),
    url(
        r'^api-token-auth/', 
        obtain_jwt_token,
        name='api-token-auth'
    )
]

#Profile URL Patterns

urlpatterns += [
    url(
        r'^profiles/$',
        ProfileView.as_view(),
        name='profile-create'
    ),
    url(
        r'^profiles/(?P<pk>[0-9]+)/$',
        ProfileDetail.as_view(),
        name='profile-detail'
    ),
    url(r'^profiles/(?P<owner_id>[0-9]+)/devices/$',DeviceCreateView.as_view(),name='device-create'),
    url(r'^profiles/(?P<owner_id>[0-9]+)/devices/(?P<pk>[0-9]+)/$',DeviceUpdateView.as_view(),name='device-update'),
    url(
        r'^profiles/(?P<owner_id>[0-9]+)/address/$',
        urls.address_list,
        name='address-list'
    ),
    url(
        r'^profiles/(?P<owner_id>[0-9]+)/address/(?P<pk>[0-9]+)/$',
        urls.address_detail,
        name='address-detail'
    ),
    url(
        r'^profiles/(?P<owner_id>[0-9]+)/teaches/$',
        urls.teaches_list,
        name='teaches-list'
    ),
    url(
        r'^profiles/(?P<owner_id>[0-9]+)/teaches/(?P<pk>[0-9]+)/$',
        urls.teaches_detail,
        name='teaches-detail'
    ),
    url(
        r'^profiles/(?P<owner_id>[0-9]+)/availability/$',
        urls.availability_list,
        name='availability-list'
    ),
    url(
        r'^profiles/(?P<owner_id>[0-9]+)/availability/(?P<pk>[0-9]+)/$',
        urls.availability_detail,
        name='availability-detail'
    ),
]

#Subject URL Patterns

urlpatterns += [
    url(
        r'^subjects/$',
        urls.subject_list,
        name='subject-list'
    ),
    url(
        r'^subjects/(?P<pk>[0-9]+)/$',
        urls.subject_detail,
        name='subject-detail'
    ),
    url(
        r'^topics/(?P<pk>[0-9]+)/$',
        urls.topic_detail,
        name='topic-detail'
    ),
    url(
        r'^lessons/(?P<pk>[0-9]+)/$',
        urls.lesson_detail,
        name='lesson-detail'
    )
]

#Appointment URL Patterns

urlpatterns += [
    url(
        r'^appointment/$',
        AppointmentView.as_view(),
        name='appointment-create'
    )
]

urlpatterns = format_suffix_patterns(urlpatterns)