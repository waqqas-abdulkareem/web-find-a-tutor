from django.http import HttpResponseBadRequest, HttpResponseNotFound

from rest_framework.renderers import JSONRenderer

def bad_request(error_dict):
	return HttpResponseBadRequest(
		content=JSONRenderer().render(error_dict),
		content_type='application/json'
	)

def not_found(error_dict):
	return HttpResponseNotFound(
		content=JSONRenderer().render(error_dict),
		content_type='application/json'
	)