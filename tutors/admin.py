from django.contrib import admin

from tutors.models.courses import *
from tutors.models.profile import *

# Register your models here.
admin.site.register(Profile)
admin.site.register(Device)
admin.site.register(Qualification)
admin.site.register(Subject)
admin.site.register(Topic)
admin.site.register(Lesson)
admin.site.register(Teaches)
admin.site.register(Address)
admin.site.register(Availability)
admin.site.register(Appointment)