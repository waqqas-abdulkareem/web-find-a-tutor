import sys

from rest_framework import serializers
from rest_framework_jwt.settings import api_settings

from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404

from tutors.models.profile import Profile, Address, Teaches, Availability, Device

from tutors.serializers import fields

#-- User ----------------------------------------------------------------------

class UserSerializer(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = ('username','first_name','last_name','email','password')
		extra_kwargs = {
            'password': {'write_only': True},
        }

class UserUpdateSerializer(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = ('first_name','last_name','email')

#-- Profile -------------------------------------------------------------------

class ProfileSerializer(serializers.ModelSerializer):
	user = UserSerializer(read_only=False)
	token = serializers.SerializerMethodField()

	class Meta:
		model = Profile
		fields = '__all__'

	def create(self, validated_data):
		user_data = validated_data.pop('user')
		user = User.objects.create(**user_data)
		user.set_password(user_data.get('password'))
		
		user.save()

		profile = Profile.objects.create(user=user,**validated_data)
		
		profile.save()

		return profile

	def get_token(self,profile):
		jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
		jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

		payload = jwt_payload_handler(profile.user)
		return jwt_encode_handler(payload)

class ProfileUpdateSerializer(serializers.ModelSerializer):
	user = UserUpdateSerializer(read_only=False)

	class Meta:
		model = Profile
		fields = '__all__'

	def update(self,  instance, validated_data):
		user_data = validated_data.pop('user')
		user = instance.user

		user.first_name = user_data.get('first_name',user.first_name)
		user.last_name = user_data.get('last_name',user.last_name)
		user.email = user_data.get('email',user.email)

		user.save()

		instance.gender = validated_data.get('gender',instance.gender)
		instance.mobile_number = validated_data.get('mobile_number',instance.mobile_number)
		instance.birthdate = validated_data.get('birthdate',instance.birthdate)
		instance.nationality = validated_data.get('nationality',instance.nationality)
		
		instance.save()

		return instance

#-- Teaches -------------------------------------------------------------------

class TeachesCreateSerializer(serializers.ModelSerializer):
	class Meta:
		model = Teaches
		fields = '__all__'
		validators = []
		read_only_fields = ('profile',)

class TeachesDetailSerializer(serializers.ModelSerializer):
	class Meta:
		model = Teaches
		exclude = ('profile',)
		depth=3

#-- Address -------------------------------------------------------------------

class AddressSerializer(serializers.ModelSerializer):
	class Meta:
		model = Address
		fields = '__all__'
		read_only_fields = ('profile',)

#-- Device --------------------------------------------------------------------

class DeviceSerializer(serializers.ModelSerializer):
	class Meta:
		model = Device
		fields = '__all__'
		read_only_fields = ('profile',)

#-- Availability --------------------------------------------------------------

class AvailabilitySerializer(serializers.ModelSerializer):
	timezone = fields.TZField()
	class Meta:
		model = Availability
		fields = '__all__'
		validators = []
		read_only_fields = ('profile',)

	def validate(self, attrs):
		instance = Availability(**attrs)
		instance.clean()
		return attrs

