import sys
from rest_framework import serializers

from tutors.models.static import STATUS_DICT
from tutors.models.courses import Lesson
from tutors.models.profile import Profile, Availability, Appointment, Teaches
from tutors.serializers.profile import ProfileSerializer, AddressSerializer, AvailabilitySerializer

class AppointmentSerializer(serializers.ModelSerializer):
	profile = serializers.SerializerMethodField()
	#addresses = serializers.SerializerMethodField()
	status = serializers.SerializerMethodField()
	availability = AvailabilitySerializer()
	price = serializers.SerializerMethodField()
	currency = serializers.SerializerMethodField()
	
	class Meta:
		model = Appointment
		exclude = ('request_id',)

	def get_profile(self, appointment):
		return ProfileSerializer(instance=appointment.availability.profile).data

	#def get_addresses(self,appointment):
	#	return AddressSerializer(instance=appointment.availability.profile.addresses,many=True).data

	def get_status(self,appointment):
		return STATUS_DICT[appointment.status]

	def get_price(self,appointment):
		profile = appointment.availability.profile
		lesson = appointment.lesson

		teaches = Teaches.objects.get(
			profile=profile,
			lesson=lesson,
		)

		return teaches.price

	def get_currency(self,appointment):
		profile = appointment.availability.profile
		lesson = appointment.lesson

		teaches = Teaches.objects.get(
				profile=profile,
				lesson=lesson
			)

		return teaches.currency