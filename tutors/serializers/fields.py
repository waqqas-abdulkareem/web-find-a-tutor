from django.utils import six
from django.utils.translation import ugettext as _

from rest_framework import serializers

class TZField(serializers.Field):
    def to_representation(self, obj):
        return six.text_type(obj)

    def to_internal_value(self,data):
        return data