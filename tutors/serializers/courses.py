from rest_framework import serializers

from tutors.models.courses import Subject, Topic, Lesson

class SubjectSerializer(serializers.ModelSerializer):
	class Meta:
		model = Subject
		fields = '__all__'

class LessonSerializer(serializers.ModelSerializer):
	class Meta:
		model = Lesson
		fields = '__all__'

class TopicSerializer(serializers.ModelSerializer):
	class Meta:
		model = Topic
		fields = '__all__'

class TopicDetailSerializer(serializers.ModelSerializer):
	lessons = LessonSerializer(many=True,read_only=True)
	class Meta:
		model = Topic
		fields = '__all__'

class SubjectDetailSerializer(serializers.ModelSerializer):
	topics = TopicSerializer(many=True,read_only=True)
	class Meta:
		model = Subject
		fields = '__all__'