import tutors.models.static

from datetime import datetime, timedelta

from django.db import IntegrityError
from django.conf import settings
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse

from rest_framework import status
from rest_framework.test import APITestCase

from tutors.models.profile import Profile
from tutors.models.courses import *
from tutors.serializers.profile import ProfileSerializer

class ProfleTests(APITestCase):
    def setUp(self):
    	self.superuser = User.objects.create_superuser('john', 'john@snow.com', 'johnpassword')
        self.client.login(username='john', password='johnpassword')
        self.data = {
        	'gender':'M',
        	'mobile_number':'+971556677889',
        	'birthdate':'1990-01-01',
        	'nationality':'AE',
        	'user':{
        		'first_name':'Jack',
        		'last_name':'Torrence',
        		'email':'jtorrence@gmail.com',
        		'username':'jack_torrence',
        		'password':'jackpassword'
        	}
        }

    def save_data_as_profile(self):
    	profile = ProfileSerializer(data=self.data)
    	if profile.is_valid():
    		profile.save()

    def test_can_create_user(self):
        response = self.client.post(reverse('profile-create'), self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Profile.objects.count(),1)
        self.assertEqual(Profile.objects.get().user.email,'jtorrence@gmail.com')
        self.assertNotIn('password',response.data['user'])

    def test_can_login(self):
    	self.save_data_as_profile()
    	data = {
    		'username':'jack_torrence',
    		'password':'jackpassword'
    	}
    	response = self.client.post(reverse('api-token-auth'),data, format='json')
    	self.assertEqual(response.status_code, status.HTTP_200_OK)
    	self.assertIn('token',response.data)

    def test_can_read_user(self):
    	self.save_data_as_profile()
    	
    	response = self.client.get(reverse('profile-detail',args=[1]))
    	self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_update_user(self):
    	self.save_data_as_profile()

    	new_email = 'jtorrence2@gmail.com'
    	self.data['user']['email'] = new_email
    	response = self.client.put(reverse('profile-detail',args=[1]),self.data,format='json')
    	updated_profile = ProfileSerializer(data=response.data)
    	self.assertEqual(response.status_code, status.HTTP_200_OK)
    	self.assertEqual(response.data['user']['email'],new_email)

    def test_can_delete_user(self):
    	self.save_data_as_profile()
    	response = self.client.delete(reverse('profile-detail',args=[1]))
    	self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

class SubjectTests(APITestCase):
	def setUp(self):
		self.superuser = User.objects.create_superuser('john', 'john@snow.com', 'johnpassword')
		self.client.login(username='john', password='johnpassword')
        
		self.subject = Subject.objects.create(name='Mathematics')
		self.topic = Topic.objects.create(name='Arithmatic',description='',subject=self.subject)
		self.lesson = Lesson.objects.create(name='Addition',description='',topic=self.topic,level=static.LEVEL_PRIMARY)

	def test_can_get_subjects(self):
		response = self.client.get(reverse('subject-list'))
		self.assertEqual(response.status_code,status.HTTP_200_OK)
		self.assertEqual(len(response.data),1)
		self.assertEqual(response.data[0]['name'],'Mathematics')
		self.assertNotIn('topics',response.data[0])

	def test_can_get_subject_detail(self):
		response = self.client.get(reverse('subject-detail',args=[1]))

		self.assertEqual(response.status_code,status.HTTP_200_OK)
		self.assertEqual(response.data['name'],'Mathematics')
		self.assertIn('topics',response.data)
		self.assertNotIn('lessons',response.data['topics'][0])
		self.assertEqual(len(response.data['topics']),1)
		self.assertEqual(response.data['topics'][0]['name'],'Arithmatic')

	def test_can_get_topic_detail(self):
		response = self.client.get(reverse('topic-detail',args=[1]))

		self.assertEqual(response.status_code,status.HTTP_200_OK)
		self.assertEqual(response.data['name'],'Arithmatic')
		self.assertIn('lessons',response.data)
		self.assertEqual(len(response.data['lessons']),1)
		self.assertEqual(response.data['lessons'][0]['name'],'Addition')

class TeachesTest(APITestCase):

	def setUp(self):
		self.superuser = User.objects.create_superuser('john', 'john@snow.com', 'johnpassword')
		self.superprofile = Profile.objects.create(
			user=self.superuser,
			birthdate='1970-01-01',
			mobile_number='971551234567',
			gender='M',
			nationality='AE'
		)
		self.otheruser = User.objects.create_superuser('ted', 'ted@bed.com', 'tedpassword')
		self.otherprofile = Profile.objects.create(
			user=self.otheruser,
			birthdate='1970-01-01',
			mobile_number='971551234568',
			gender='M',
			nationality='AE'
		)
		self.client.login(username='john', password='johnpassword')

		self.subject = Subject.objects.create(name='Mathematics')
		self.topic = Topic.objects.create(name='Arithmatic',description='',subject=self.subject)
		self.addition = Lesson.objects.create(name='Addition',description='',topic=self.topic,level=static.LEVEL_PRIMARY)
		self.subtraction = Lesson.objects.create(name='Subtraction',description='',topic=self.topic,level=static.LEVEL_PRIMARY)
		
		self.data = [
			{'lesson':self.addition.id},
			{'lesson':self.subtraction.id}
		]

	def test_can_post_multiple_teaches(self):
		response = self.client.post(reverse('teaches-list',args=[self.superprofile.id]),self.data,format='json')
		self.assertEqual(response.status_code,status.HTTP_201_CREATED)

	def test_cant_post_multiple_teaches_on_other_user(self):
		response = self.client.post(reverse('teaches-list',args=[self.otherprofile.id]),self.data,format='json')
		self.assertEqual(response.status_code,status.HTTP_403_FORBIDDEN)

class AvailabilityTest(APITestCase):

	def setUp(self):
		self.superuser = User.objects.create_superuser('john', 'john@snow.com', 'johnpassword')
		self.client.login(username='john', password='johnpassword')

		self.profile = self.create_profile()
		self.data = {
			'day_of_week':'Sat',
			'starting_time':'10:00',
			'ending_time':'12:00',
			'timezone':'Asia/Dubai'
		}

	def create_profile(self):
		data = {
			'gender':'M',
			'mobile_number':'+971556677889',
			'birthdate':'1990-01-01',
			'nationality':'AE',
			'user':{
				'first_name':'Jack',
				'last_name':'Torrence',
				'email':'jtorrence@gmail.com',
				'username':'jack_torrence',
				'password':'jackpassword'
			}
		}
		profile = ProfileSerializer(data = data)

		if profile.is_valid():
			return profile.save()
		return None

	def test_can_create_availability(self):
		response = self.client.post(reverse('availability-list',args=[1]), self.data, format='json')
		print response.content
		self.assertEqual(response.status_code,status.HTTP_201_CREATED)

	def test_can_create_availability(self):
		self.client.post(reverse('availability-list',args=[1]), self.data, format='json')
		data = {
			'day_of_week':'Sat',
			'starting_time':'10:00',
			'ending_time':'14:00',
			'timezone':'Asia/Dubai'
		}
		with self.assertRaises(IntegrityError):
			response = self.client.post(reverse('availability-list',args=[1]), data, format='json')
			

	def test_ending_time_before_starting_time(self):
		self.data['ending_time'] = '9:00'

		response = self.client.post(reverse('availability-list',args=[1]),self.data,format='json')
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

	def test_starting_time_before_min_starting_time(self):
		too_early_date = datetime.datetime.combine(datetime.datetime.now(),settings.MINIMUM_STARTING_TIME) - timedelta(hours=1)
		self.data['starting_time'] = too_early_date.strftime('%H:%M')

		response = self.client.post(reverse('availability-list',args=[1]),self.data,format='json')
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

	def test_ending_time_after_max_ending_time(self):
		too_late_date = datetime.datetime.combine(datetime.datetime.now(),settings.MAXIMUM_ENDING_TIME) + timedelta(hours=1)
		self.data['ending_time'] = too_late_date.strftime('%H:%M')

		response = self.client.post(reverse('availability-list',args=[1]),self.data,format='json')
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

	def test_duration_less_than_minimum(self):
		starting_time = datetime.datetime.strptime(self.data['starting_time'],'%H:%M')
		too_late_date = datetime.datetime.combine(datetime.datetime.now(),starting_time.time()) + timedelta(hours=(settings.MINIMUM_AVAILABILITY_DURATION/2))
		self.data['ending_time'] = too_late_date.strftime('%H:%M')

		response = self.client.post(reverse('availability-list',args=[1]),self.data,format='json')
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

class DeviceTest(APITestCase):

	def setUp(self):
		self.superuser = User.objects.create_superuser('john', 'john@snow.com', 'johnpassword')
		self.profile = Profile.objects.create(
			user=self.superuser,
			birthdate='1970-01-01',
			mobile_number='971551234567',
			gender='M',
			nationality='AE'
		)
		self.client.login(username='john', password='johnpassword')

		self.data = {
			'token':'4066cfdc9aebc36985d11ee7b5af75024eaf5532155a48c7084726b236efda93',
			'os':'ios',
			'version':'9.0.1'
		}

	def test_can_create_device(self):
		response = self.client.post(reverse('device-create',args=[self.profile.id]),self.data,format='json')
		self.assertEqual(response.status_code,status.HTTP_201_CREATED)
		self.assertEquals(response.data['id'],1)
		self.assertEquals(response.data['os'],'ios')

	def test_can_update_device(self):
		self.client.post(reverse('device-create',args=[self.profile.id]),self.data,format='json')
		self.data['os'] = 'android'
		self.data['version'] = '4.1.1'
		self.data['token'] = '294913EC-6100-42E8-8C2D-E9F68F286ADE'
		response = self.client.put(reverse('device-update',args=[self.profile.id,1]),self.data,format='json')
		self.assertEqual(response.status_code,status.HTTP_200_OK)
		self.assertEquals(response.data['id'],1)
		self.assertEquals(response.data['os'],'android')
		