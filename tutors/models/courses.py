import datetime
import static
from django.db import models

class Subject(models.Model):
	name 		= models.CharField(unique=True,max_length=100)

	def __str__(self):
		return self.name

class Topic(models.Model):
	name 		= models.CharField(unique=True,max_length=100)
	description	= models.CharField(max_length=300)
	subject 	= models.ForeignKey(Subject, on_delete=models.CASCADE, related_name='topics')

	def __str__(self):
		return self.name

class Lesson(models.Model):
	name 				= models.CharField(unique=True,max_length=100)
	description			= models.TextField()
	topic 				= models.ForeignKey(Topic, on_delete=models.CASCADE,related_name='lessons')
	duration			= models.DurationField(default=datetime.timedelta(hours=1))
	level				= models.PositiveSmallIntegerField(choices=static.LEVEL_CHOICES)

	def __str__(self):
		return '%s - %s' % (static.LEVEL_DICT[self.level],self.name)

	class Meta:
		unique_together = (
			('name','level'),
		)