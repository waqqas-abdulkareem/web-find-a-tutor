#-- Course --------------------------------------------------------------------
#Based on ISCED 2011 Levels of Education, defined by UNESCO

LEVEL_PRE_PRIMARY = 0 
LEVEL_PRIMARY = 100 

#GCSE
LEVEL_LOWER_SECONDARY = 200 

#A-Levels
LEVEL_UPPER_SECONDARY = 300 

LEVEL_BACHELOR = 400

#Unlisted
LEVEL_MASTER = 500

#Unlisted (A PhD student who needs tuitions? Seriously?)
LEVEL_DOCTORATE = 600

LEVEL_CHOICES = (
    (LEVEL_PRE_PRIMARY, "Early Childhood (Ages 3-5) "),
    (LEVEL_PRIMARY,     "Primary (Ages 5-11)"),
    (LEVEL_LOWER_SECONDARY, "Lower Secondary (Ages 12-15)"),
    (LEVEL_UPPER_SECONDARY, "Upper Secondary (Ages 16-18)"),
    (LEVEL_BACHELOR, "Undergraduate"),
)

LEVEL_DICT = dict((k, v) for k, v in LEVEL_CHOICES)

#-- Profile -------------------------------------------------------------------

GENDER_CHOICES 	= (
    ('M', 'Male'),
    ('F', 'Female'),
)

PLATFORM_CHOICES = (
    ('ios','iOS'),
    ('android','Android'),
)

#-- Availability --------------------------------------------------------------

DAY_OF_WEEK_CHOICES = (
    ('Sat', 'Saturday'),
    ('Sun', 'Sunday'),
    ('Mon', 'Monday'),
    ('Tue', 'Tuesday'),
    ('Wed', 'Wednesday'),
    ('Thu', 'Thursday'),
    ('Fri', 'Friday')
)

#-- Appointment ---------------------------------------------------------------

STATUS_AWAITING_TUTOR_SELECTION = 100
STATUS_AWAITING_TUTOR_CONFIRMATION = 200
STATUS_SCHEDULED = 300

STATUS_CHOICES = (
	(STATUS_AWAITING_TUTOR_SELECTION,		'AwaitingTutorSelection'),
	(STATUS_AWAITING_TUTOR_CONFIRMATION,	'AwaitingTutorConfirmation'),
	(STATUS_SCHEDULED,						'Scheduled'),
)

STATUS_DICT = dict((k, v) for k, v in STATUS_CHOICES)