from __future__ import unicode_literals

import sys
import datetime as dt
from decimal import Decimal
from moneyed import Money

from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from django.core.validators import RegexValidator, MinLengthValidator,MaxValueValidator, MinValueValidator
from django.core.exceptions import ValidationError
from django_countries.fields import CountryField

from djmoney.models.fields import CurrencyField
from djmoney.settings import CURRENCY_CHOICES

from timezone_field import TimeZoneField

from courses import *
from static import GENDER_CHOICES, DAY_OF_WEEK_CHOICES, STATUS_CHOICES, STATUS_DICT, PLATFORM_CHOICES

class Profile(models.Model):
	phone_regex 	= RegexValidator(
		regex=r'^\+?\d{9,15}$', 
		message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed."
	)
	skype_regex		= RegexValidator(
		regex=r'/^[a-z][a-z0-9\.,\-_]{5,31}$',
		message="Invalid Skype name"
	)
	bio_length 		= MinLengthValidator(
		60,
		message		= "Biography should be atleast 60 characters long"
	)
	user 			= models.OneToOneField(User, on_delete=models.CASCADE)
	birthdate 		= models.DateField()
	mobile_number 	= models.CharField(unique=True,max_length=15,validators=[phone_regex])
	gender 			= models.CharField(max_length=1, choices=GENDER_CHOICES)
	nationality 	= CountryField()
	skype_id		= models.CharField(max_length=32,unique=True,blank=False,null=True,validators=[skype_regex])
	tutors_online	= models.BooleanField(default=False)
	travel_limit_km	= models.PositiveSmallIntegerField(default=0,blank=False,null=True)
	biography 		= models.CharField(max_length=255,blank=False,null=True,validators=[bio_length])

	def owner(self):
		return self.user

	def __str__(self):
		return  '%s %s' % (
			self.user.first_name,
			self.user.last_name
		)

class Device(models.Model):
	profile 	= models.ForeignKey(Profile, on_delete=models.CASCADE, related_name='devices')
	# Token Storage Size Discussion: https://groups.google.com/forum/#!msg/android-gcm/q2PzJTP71TY/YUKmCjx5kKwJ
	# iOS Token is 64 characters
	# Android Token can be upto 255 characters
	token		= models.CharField(max_length=256,blank=False,null=True)
	os			= models.CharField(max_length=128,choices=PLATFORM_CHOICES,blank=False,null=False)
	version 	= models.CharField(max_length=128,blank=False,null=True)

	def owner(self):
		return self.profile.user

	def __str__(self):
		return '%s - %s %s - Token: %s' % (
			self.profile, 
			self.os,
			self.version, 
			self.token
		)

class Qualification(models.Model):
	profile 	= models.ForeignKey(Profile, on_delete=models.CASCADE, related_name='qualifications')
	subject		= models.CharField(max_length=255)
	certificate	= models.CharField(max_length=255)
	authority 	= models.CharField(max_length=255)
	year		= models.PositiveSmallIntegerField(validators=[MinValueValidator(1970),MaxValueValidator(2038)])

	def owner(self):
		return self.profile.user

	def __str__(self):
		return '%s - %s in %s from %s %d' % (
			self.profile,
			self.certificate,
			self.subject,
			self.authority,
			self.year
		)

class Address(models.Model):
	profile		= models.ForeignKey(Profile,on_delete=models.CASCADE,related_name='addresses')
	line_one 	= models.CharField(max_length=128)
	line_two 	= models.CharField(max_length=128,blank=True,null=True)
	city 		= models.CharField(max_length=100)
	state 		= models.CharField(max_length=100,blank=True, null=True)
	country 	= CountryField()
	latitude 	= models.FloatField(blank=False,null=True)
	longitude 	= models.FloatField(blank=False,null=True)

	def owner(self):
		return self.profile.user

class Teaches(models.Model):
	profile 	= models.ForeignKey(Profile,on_delete=models.CASCADE, related_name='teaches')
	lesson 		= models.ForeignKey(Lesson,on_delete=models.CASCADE)
	price		= models.DecimalField(
					default=Decimal('0'),
					max_digits=settings.CURRENCY_MAX_DIGITS,
					decimal_places=settings.CURRENCY_DECIMAL_PLACES,
				  )
	currency 	= CurrencyField(default=settings.CURRENCY_DEFAULT,choices=CURRENCY_CHOICES)

	class Meta:
		unique_together = (
			('profile','lesson'),
		)

	def owner(self):
		return self.profile.user

	def __str__(self):
		return '%s %s - %s' % (
			self.profile.user.first_name, 
			self.profile.user.last_name,
			self.lesson.name
		)

class Availability(models.Model):
	profile			= models.ForeignKey(Profile,on_delete=models.CASCADE,related_name='availability')
	day_of_week 	= models.CharField(max_length=3,choices=DAY_OF_WEEK_CHOICES)
	starting_time 	= models.TimeField()
	ending_time 	= models.TimeField()
	timezone 		= TimeZoneField()

	class Meta:
		#http://stackoverflow.com/q/34103483/821110
		unique_together = (
			('profile','day_of_week','timezone','starting_time'),
			('profile','day_of_week','timezone','ending_time')
		)

	def owner(self):
		return self.profile.user

	def __str__(self):
		return "%s - %s %s - %s %s" % (
			self.profile.id,
			self.day_of_week, 
			self.starting_time, 
			self.ending_time, 
			self.timezone
		)

	def clean(self):
		''' Django Rest Framework's documentation recommneds that validation methods
		be written on the serializer. However, this prevents validation from taking
		place when the models are edited from the admin panel.
		Therefore, it's better to associate validation directly with the model '''

		validation_errors = {}
		
		minimum_starting_time = settings.MINIMUM_STARTING_TIME
		maximum_ending_time = settings.MAXIMUM_ENDING_TIME
		min_duration = settings.MINIMUM_AVAILABILITY_DURATION

		if self.starting_time < minimum_starting_time:
			validation_errors['starting_time'] = ValidationError(
				'Starting time can not be earlier than %(min)s',
				params = {'min': minimum_starting_time},
				code = 'invalid'
			)
		if self.ending_time > maximum_ending_time:
			validation_errors['ending_time'] = ValidationError(
				'Ending time can not be later than %(max)s',
				params = {'max': maximum_ending_time},
				code = 'invalid'
			)

		interval_in_seconds = ((dt.datetime.combine(dt.date.today(),self.ending_time) - dt.datetime.combine(dt.date.today(),self.starting_time))).total_seconds()

		if (interval_in_seconds / 3600) < min_duration:
			validation_errors['ending_time'] = ValidationError(
				'Ending time must be %(min_duration)d hours after starting time',
				params = {'min_duration': min_duration},
				code = 'invalid'
			)
		if validation_errors:
			raise ValidationError(validation_errors)


class Appointment(models.Model):
	availability 	= models.ForeignKey(Availability,on_delete=models.PROTECT,related_name='appointments') 
	student 		= models.ForeignKey(Profile,on_delete=models.CASCADE,related_name='appointments')
	lesson 			= models.ForeignKey(Lesson,on_delete=models.PROTECT,related_name='appointments')
	date 			= models.DateTimeField()
	status 			= models.PositiveSmallIntegerField(choices=STATUS_CHOICES)
	request_id		= models.UUIDField(unique=False, blank=False, null=True)

	def owner(self):
		return self.availability.profile.user

	def __str__(self):
		return "Lesson:%s, Student: %s, Teacher: %s, Date: %s [Status: %s]" % (
			self.lesson,
			self.student,
			self.owner(),
			self.date,
			STATUS_DICT[self.status]
		)

	class Meta:
		unique_together = (
			('availability','date'),
		)