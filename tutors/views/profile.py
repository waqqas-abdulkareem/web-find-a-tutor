import sys
import json
import logging

from django.conf import settings
from django.http import HttpResponseBadRequest
from django.shortcuts import get_object_or_404
from django.contrib.auth.hashers import make_password

from rest_framework import mixins
from rest_framework import generics
from rest_framework import status
from rest_framework import mixins
from rest_framework import viewsets
from rest_framework.reverse import reverse
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny

from tutors.models.courses import *
from tutors.models.profile import *
from tutors.serializers.courses import * 
from tutors.serializers.profile import *
from tutors.views.permissions import IsStaffOrAuthenticated, IsStaffOrOwner

def jwt_response_payload_handler(token, user=None, request=None):
	profile = get_object_or_404(Profile,user=user)
	#Use ProfileSerializer because ProfileUpdateSerializer does not include username
	data = ProfileSerializer(profile).data
	data['token'] = token
	return data

class ProfileView(generics.GenericAPIView,mixins.CreateModelMixin):
	serializer_class = ProfileSerializer
	permission_classes = (AllowAny,)

	def post(self, request, *args, **kwargs):
		return self.create(request, *args, **kwargs)

class ProfileDetail(generics.RetrieveUpdateDestroyAPIView):
	queryset = Profile.objects.all()
	serializer_class = ProfileUpdateSerializer
	permission_classes = (IsStaffOrOwner,)

class AddressViewSet(viewsets.ModelViewSet):
	serializer_class = AddressSerializer
	permission_classes = (IsStaffOrOwner,)

	def get_queryset(self):
		profile = get_object_or_404(Profile,id=self.kwargs['owner_id'])
		self.check_object_permissions(self.request, profile)
		return Address.objects.filter(profile=profile)

	def perform_create(self,serializer):
		profile = get_object_or_404(Profile,id=self.kwargs['owner_id'])
		self.check_object_permissions(self.request, profile)
		serializer.save(profile=profile)

class DeviceCreateView(generics.GenericAPIView, mixins.CreateModelMixin):
	serializer_class = DeviceSerializer
	permission_classes = (IsStaffOrOwner,)

	def post(self, request, *args, **kwargs):
		return self.create(request, *args, **kwargs)

	def perform_create(self,serializer):
		profile = get_object_or_404(Profile, id=self.kwargs['owner_id'])
		self.check_object_permissions(self.request,profile)
		serializer.save(profile=profile)

class DeviceUpdateView(generics.GenericAPIView, mixins.UpdateModelMixin):
	serializer_class = DeviceSerializer
	permission_classes = (IsStaffOrOwner,)

	def put(self,request,*args,**kwargs):
		return self.update(request, *args, **kwargs)

	def get_queryset(self):
		profile = get_object_or_404(Profile,id=self.kwargs['owner_id'])
		self.check_object_permissions(self.request, profile)
		return Device.objects.filter(profile=profile)

	def perform_update(self,serializer):
		profile = get_object_or_404(Profile, id=self.kwargs['owner_id'])
		self.check_object_permissions(self.request,profile)
		serializer.save()

#TODO: Permit multiple teaches to be submitted at once
class TeachesViewSet(viewsets.ModelViewSet):
	permission_classes = (IsStaffOrOwner,)

	def get_queryset(self):
		profile = get_object_or_404(Profile,id=self.kwargs['owner_id'])
		self.check_object_permissions(self.request, profile)
		return Teaches.objects.filter(profile=profile)

	def create(self, request, *args, **kwargs):
		profile = get_object_or_404(Profile,id=kwargs['owner_id'])
		self.check_object_permissions(self.request, profile)
		serializer = self.get_serializer(data=request.data, many=True)
		if serializer.is_valid():
			serializer.save(profile=profile)
			headers = self.get_success_headers(serializer.data)
			return Response(
				serializer.data, 
				status=status.HTTP_201_CREATED,
				headers=headers
			)

		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

	def get_serializer_class(self):
		if self.action == 'create':
			return TeachesCreateSerializer
		return TeachesDetailSerializer


class AvailabilityViewSet(viewsets.ModelViewSet):
	serializer_class = AvailabilitySerializer
	permission_classes = (IsStaffOrOwner,)

	def get_queryset(self):
		profile = get_object_or_404(Profile,id=self.kwargs['owner_id'])
		self.check_object_permissions(self.request, profile)
		return Availability.objects.filter(profile=profile)

	def perform_create(self,serializer):
		profile = get_object_or_404(Profile,id=self.kwargs['owner_id'])
		self.check_object_permissions(self.request, profile)
		serializer.save(profile=profile)