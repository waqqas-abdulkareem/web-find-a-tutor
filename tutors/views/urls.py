from tutors.views.courses import *
from tutors.views.profile import *

subject_list = SubjectViewSet.as_view({
	'get':		'list'
})
subject_detail = SubjectViewSet.as_view({
	'get':		'retrieve'
})
topic_detail = TopicViewSet.as_view({
	'get':		'retrieve'
})
lesson_detail= LessonViewSet.as_view({
	'get':		'retrieve'
})
teaches_list = TeachesViewSet.as_view({
	'get':		'list',
	'post':		'create'
})
teaches_detail = TeachesViewSet.as_view({
	'get':		'retrieve',
	'delete':	'destroy'
})
address_list = AddressViewSet.as_view({
	'get':		'list',
	'post':		'create'
})
address_detail = AddressViewSet.as_view({
	'get':		'retrieve',
	'put':		'update',
	'delete':	'destroy'
})
availability_list = AvailabilityViewSet.as_view({
	'post':		'create',
	'get':		'list'	
})
availability_detail = AvailabilityViewSet.as_view({
	'get':		'retrieve',
	'put':		'update',
	'delete':	'destroy'
})