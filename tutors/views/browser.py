from rest_framework.reverse import reverse
from rest_framework.response import Response
from rest_framework.decorators import api_view

@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'subjects': reverse('subject-list', request=request, format=format),
        'topics': reverse('topic-detail', kwargs={'pk':request.user.id}, request=request, format=format),
        'lessons': reverse('lesson-detail', kwargs={'pk':request.user.id}, request=request, format=format),
    	'profile': reverse('profile-create', request=request, format=format),
    	'address': reverse('address-list', kwargs={'owner_id':request.user.id},request=request, format=format),
    	'teaches': reverse('teaches-list', kwargs={'owner_id':request.user.id},request=request, format=format),
    	'availability': reverse('availability-list', kwargs={'owner_id':request.user.id},request=request, format=format)
    })