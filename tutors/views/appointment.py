import uuid
import random
import logging

from sets import Set
from dateutil import parser
from pytz import timezone,all_timezones

from django.http import Http404
from django.db.models import Q

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
from rest_framework.permissions import IsAuthenticated

from tutors.models.courses import *
from tutors.models.profile import *
from tutors.serializers.profile import * 
from tutors.serializers.appointment import *
from tutors.shortcuts import bad_request, not_found
from tutors.models.static import STATUS_AWAITING_TUTOR_SELECTION

class AppointmentView(APIView):
	permission_classes = (IsAuthenticated,)

	def post(self, request, format='json'):
		profile 			= Profile.objects.get(user__id=request.user.id)
		lesson 				= None
		address 			= None
		timezone_name 		= None
		date 				= None
		appointment_time	= None

		try:
			lesson = Lesson.objects.get(id=int(request.data['lesson']))
		except KeyError:
			return bad_request({'lesson': 'lesson missing from request body'})
		except Lesson.DoesNotExist:
			raise Http404('A lesson with the given id does not exist')
		
		try:
			address = Address.objects.get(id=int(request.data['address']))
			if address.profile.id is not profile.id:
				return bad_request({'address':'This address does not belong to the user'})
		except KeyError:
			return bad_request({'address':'address missing from request body'})
		except Address.DoesNotExist:
			raise Http404('An address with given id does not exist')

		try:
			sdate = request.data['date']
			date = parser.parse(sdate)
		except KeyError:
			return bad_request({'date':'date missing from request body'})

		try:
			_timezone_name = request.data['timezone']
			if _timezone_name not in all_timezones:
				return bad_request({'timezone':'%s is not a recognized timezone'%_timezone_name})
			timezone_name = _timezone_name
		except KeyError:
			return bad_request({'timezone':'timezone missing from request body'})
		
		tz = timezone(timezone_name)
		appointment_time= tz.localize(date)

		#starting_time 		= appointment_time.strftime('%H:%M:%S')
		#ending_time 		= (appointment_time + lesson.duration).strftime('%H:%M:%S')
		day_of_week 		= appointment_time.strftime('%a')

		logging.info('''Params:\n
			timezone: %s\n
			day_of_week: %s\n''' % 
			(repr(timezone_name),
			repr(day_of_week),)
		)		

		availabilities = Availability.objects.filter(
			#Q(starting_time__lte=starting_time) &
			#Q(ending_time__gte=ending_time) &
			Q(timezone=timezone_name) &
			Q(day_of_week=day_of_week) &
			Q(profile__teaches__lesson=lesson) & 
			~Q(appointments__date__range=(appointment_time,appointment_time+lesson.duration))
		).exclude(
			profile=profile
		)
		
		if availabilities:
			appointments = []
			random_48_bits = random.randint(0,2 ** 48 - 1)
			request_id = uuid.uuid1(random.randint(0,random_48_bits))
			
			for availability in availabilities:
				appointment = Appointment.objects.create(
                    availability    = availability,
                    student         = profile,
                    lesson          = lesson,
                    date            = appointment_time,
                    status          = STATUS_AWAITING_TUTOR_SELECTION,
                    request_id      = request_id
                )

				appointments.append(appointment)

			return Response(AppointmentSerializer(instance=appointments,many=True).data)

		return not_found({'error':'No tutors available for this period'})