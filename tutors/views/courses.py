from rest_framework import viewsets

from tutors.models.courses import * 
from tutors.serializers.courses import *
from tutors.views.permissions import IsStaffOrAuthenticated

class SubjectViewSet(viewsets.ModelViewSet):
	queryset = Subject.objects.all()
	permission_classes = (IsStaffOrAuthenticated,)

	def get_serializer_class(self):
		if self.action == 'retrieve':
			return SubjectDetailSerializer
		return SubjectSerializer

class TopicViewSet(viewsets.ModelViewSet):
	queryset = Topic.objects.all()
	serializer_class = TopicDetailSerializer
	permission_classes = (IsStaffOrAuthenticated,)

class LessonViewSet(viewsets.ModelViewSet):
	queryset = Lesson.objects.all()
	serializer_class = LessonSerializer
	permission_classes = (IsStaffOrAuthenticated,)